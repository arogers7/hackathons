import requests
from datetime import datetime
def createtextmessage():
    key = '7bea8ae47a9ab71d001ff6cd655f852c'
    coor1 = '42.8864'
    coor2 = '-78.8784'
    url = 'https://api.darksky.net/forecast/{}/{},{}'.format(key,coor1,coor2)
    r = requests.get(url)
    usabledata = r.json()
    sevenandthree = []
    for data in usabledata['hourly']['data']:
        date = datetime.utcfromtimestamp(data['time'])
        if (date.hour == 11 and len(sevenandthree) == 0):
            sevenandthree.append(data)
        if (date.hour == 19 and len(sevenandthree) == 1):
            sevenandthree.append(data)
    message = open('textmessage.txt','r').read().format(sevenandthree[0]['apparentTemperature'],str(sevenandthree[0]['precipProbability']*100),str(sevenandthree[1]['precipProbability']))
    return message
