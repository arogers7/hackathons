from datetime import timedelta
from celery.schedules import crontab


class CeleryConfig:

    BACKEND = 'amqp://'
    CELERYBEAT_SCHEDULE = {
        'query': {
            'task': 'tasks.query',
            'schedule': crontab(hour=20, minute=18)
        }
    }
    CELERY_TIMEZONE = 'US/Eastern'


class AppConfig:

    WEATHER_API_KEY = '7bea8ae47a9ab71d001ff6cd655f852c'
    WEATHER_API_URI_TEMPLATE = 'https://api.darksky.net/forecast/{}/{},{}'
    LOCATION = dict(
        latitude='42.8864',
        longitude='-78.8784'
    )