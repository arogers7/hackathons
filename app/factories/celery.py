from celery import Celery


def make_celery(object_str='config.CeleryConfig'):
    app = Celery()
    app.config_from_object(object_str)
    return app