import requests
from config import AppConfig


def fetch_weather():
    api_key = AppConfig.WEATHER_API_KEY
    uri_temp = AppConfig.WEATHER_API_URI_TEMPLATE
    location = AppConfig.LOCATION
    url = uri_temp.format(api_key, location['latitude'], location['longitude'])
    r = requests.get(url)
    resp_json = r.json()

    return resp_json

