# Timestamp localization
def timestamptolocal(timestamp):
    from datetime import datetime
    from pytz import timezone
    eastern = timezone('US/Eastern')

    date = datetime.utcfromtimestamp(timestamp)
    return eastern.localize(date)