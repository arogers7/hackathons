from app.factories.celery import make_celery
from app.util import timestamptolocal
celery = make_celery()

@celery.task(name='tasks.query')
def query_weather():
    from app.weather_api import fetch_weather

    weather_data = fetch_weather()
    hourly = weather_data['hourly']['data']

    def checkhours(date, hours=[]):
        from datetime import datetime, timedelta
        # Current day
        now = datetime.utcnow()
        # Day timedelta
        day_td = timedelta(days=1)
        # Substract date from now
        td = date.replace(tzinfo=None) - now
        if td > day_td:
            return False
        date_hour = date.hour
        for hour in hours:
            if date_hour == hour:
                return True
        return False

    filtered_hours = list(filter(lambda o: checkhours(timestamptolocal(o['time']), [7, 3]), hourly))

    celery.signature('tasks.email', args=(filtered_hours,)).apply_async()
    celery.signature('tasks.sms', args=(filtered_hours,)).apply_async()


@celery.task(name='tasks.email')
def send_email(weather_data):
    import smtplib
    from email.mime.text import MIMEText

    address = "lonelybot1000@gmail.com"
    subject = "Today's Weather Forecast"
    client = "arogers7@buffalo.edu"

    #for launch task
    message = 'Subject: {}\n\nAt 7 am, the temperature will feel like {} degrees F, and there is a {}% chance of precipitation. At 3 pm, the chance of rain will be {}%. Stay warm!'.format("Weather update",int(weather_data[0]['temperature']),int(weather_data[0]['precipProbability']*100),int(weather_data[1]['precipProbability']*100))
    address = "lonelybot1000@gmail.com"

    server = smtplib.SMTP('smtp.gmail.com',587)
    server.starttls()
    server.login(address,'insecureaccount')
    server.sendmail(address,"arogers7@buffalo.edu",message)
    server.quit()


@celery.task(name='tasks.sms')
def send_sms(weather_data):
    print("Sending SMS...")
